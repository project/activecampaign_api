<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Account;
use Drupal\activecampaign_api\Endpoint\Accounts;

/**
 * Class AlterresourceTest.
 *
 * Tests the alter hooks.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 *
 * @group activecampaign_api
 */
class AlterresourceTest extends ActivecampaignApiKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'activecampaign_api_test_alterresource',
  ];

  /**
   * The accounts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Accounts
   */
  protected $endpoint;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Accounts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Accounts::class);
    $this->endpoint = $endpoint;
  }

  /**
   * TestAccountsCrud.
   */
  public function testAlterCreateUpdate(): void {
    // Create.
    $new_account = new Account();
    $new_account->name = 'activecampaign_api';

    $this->assertCount(0, $this->endpoint->list(['name' => $new_account->name]));
    $account = $this->endpoint->create($new_account);
    $this->assertEquals($new_account->name . 'create', $account->name);

    $list = $this->endpoint->list(['name' => $new_account->name]);
    $this->assertIsArray($list);
    $this->assertCount(0, $list);

    $list = $this->endpoint->list(['name' => $new_account->name . 'create']);
    $this->assertIsArray($list);
    $this->assertCount(1, $list);
    $this->assertEquals($new_account->name . 'create', $list[0]->name);

    // Update.
    $this->endpoint->update($account);
    $updated_account = $this->endpoint->get($account->id);
    $this->assertEquals($new_account->name . 'createupdate', $updated_account->name);

    // Delete.
    $this->endpoint->delete($account->id);
  }

}
