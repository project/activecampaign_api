<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\ApiResource\FieldValue;
use Drupal\activecampaign_api\Endpoint\Contacts;
use Drupal\activecampaign_api\Endpoint\Fields;
use Drupal\activecampaign_api\Endpoint\FieldValues;

/**
 * Class FieldValuesTest.
 *
 * Tests the FieldValues endpoint.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 *
 * @group activecampaign_api
 */
class FieldValuesTest extends ActivecampaignApiKernelTestBase {

  /**
   * The contacts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Contacts
   */
  protected $contactsEndpoint;

  /**
   * The fields endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Fields
   */
  protected $fieldsEndpoint;

  /**
   * The field values endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\FieldValues
   */
  protected $endpoint;

  /**
   * The contact.
   *
   * @var \Drupal\activecampaign_api\ApiResource\Contact
   */
  protected $contact;

  /**
   * The field.
   *
   * @var \Drupal\activecampaign_api\ApiResource\Field
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Contacts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Contacts::class);
    $this->contactsEndpoint = $endpoint;
    /**
     * @var \Drupal\activecampaign_api\Endpoint\Fields $endpoint
     */
    $endpoint = $this->endpointFactory->get(Fields::class);
    $fieldsEndpoint = $endpoint;

    /**
     * @var \Drupal\activecampaign_api\Endpoint\FieldValues $endpoint
     */
    $endpoint = $this->endpointFactory->get(FieldValues::class);
    $this->endpoint = $endpoint;

    $this->contact = new Contact();
    $this->contact->email = 'activecampaign_api@groowup.nl';
    $this->contact = $this->contactsEndpoint->create($this->contact);

    $this->field = $fieldsEndpoint->get(1);
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    $this->contactsEndpoint->delete($this->contact->id);
    parent::tearDown();
  }

  /**
   * TestCrud.
   */
  public function testCrud(): void {
    $this->assertNull($this->endpoint->getByContactField($this->contact, $this->field));

    $field_value = new FieldValue($this->field->id, '12345', $this->contact->id);
    $field_value = $this->endpoint->create($field_value);
    $this->assertInstanceOf(FieldValue::class, $field_value);
    $this->assertNotEmpty($field_value->id);
    $this->assertEquals('12345', $field_value->value);

    $field_value_get = $this->endpoint->getByContactField($this->contact, $this->field);
    $this->assertInstanceOf(FieldValue::class, $field_value_get);
    $this->assertEquals($field_value->id, $field_value_get->id);
    $this->assertEquals($field_value->value, $field_value_get->value);

    $field_value->value = '654321';
    $this->endpoint->update($field_value);

    $field_value_get = $this->endpoint->getByContactField($this->contact, $this->field);
    $this->assertInstanceOf(FieldValue::class, $field_value_get);
    $this->assertEquals($field_value->id, $field_value_get->id);
    $this->assertEquals('654321', $field_value_get->value);

    $this->endpoint->delete($field_value->id);
    $this->assertNull($this->endpoint->getByContactField($this->contact, $this->field));
  }

}
