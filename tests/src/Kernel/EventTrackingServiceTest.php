<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\Endpoint\Contacts;

/**
 * Class EventTrackingServiceTest.
 *
 * Tests the EventTrackingService class.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 * @group activecampaign_api
 */
class EventTrackingServiceTest extends ActivecampaignApiKernelTestBase {

  /**
   * The event tracking service.
   *
   * @var \Drupal\activecampaign_api\Service\EventTrackingService
   */
  protected $eventTrackingService;

  /**
   * The Contacts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Contacts
   */
  protected $contactsEndpoint;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->eventTrackingService = $this->container->get('activecampaign_api.event_tracking');

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Contacts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Contacts::class);
    $this->contactsEndpoint = $endpoint;
  }

  /**
   * TestTrack.
   */
  public function testTrack(): void {
    // Create a test contact.
    $contact = new Contact();
    $contact->email = 'activecampaign_api@groowup.nl';
    $this->assertCount(0, $this->contactsEndpoint->list(['email' => $contact->email]));
    $contact = $this->contactsEndpoint->create($contact);

    // Track an event.
    $this->eventTrackingService->track($this->activecampaignApiAccount, 'BHVNieuw', $contact->email, 'data');

    // Track an event for a non-existing contact.
    $this->eventTrackingService->track($this->activecampaignApiAccount, 'BHVNieuw', 'nonexisting@groowup.nl');

    $this->contactsEndpoint->delete($contact->id);
  }

}
