<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\ApiResource\ContactTag;
use Drupal\activecampaign_api\ApiResource\Tag\Contact as TagContact;
use Drupal\activecampaign_api\Endpoint\Contacts;
use Drupal\activecampaign_api\Endpoint\ContactTags;
use Drupal\activecampaign_api\Endpoint\Tags;

/**
 * Class ContactTagsTest.
 *
 * Tests the ContactTags endpoint.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 *
 * @group activecampaign_api
 */
class ContactTagsTest extends ActivecampaignApiKernelTestBase {

  /**
   * The contacts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Contacts
   */
  protected $contactsEndpoint;

  /**
   * The tags endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Tags
   */
  protected $tagsEndpoint;

  /**
   * The contact tags endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\ContactTags
   */
  protected $endpoint;

  /**
   * The contact.
   *
   * @var \Drupal\activecampaign_api\ApiResource\Contact
   */
  protected $contact;

  /**
   * The tag.
   *
   * @var \Drupal\activecampaign_api\ApiResource\Tag
   */
  protected $tag;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Contacts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Contacts::class);
    $this->contactsEndpoint = $endpoint;
    /**
     * @var \Drupal\activecampaign_api\Endpoint\Tags $endpoint
     */
    $endpoint = $this->endpointFactory->get(Tags::class);
    $this->tagsEndpoint = $endpoint;

    /**
     * @var \Drupal\activecampaign_api\Endpoint\ContactTags $endpoint
     */
    $endpoint = $this->endpointFactory->get(ContactTags::class);
    $this->endpoint = $endpoint;

    $this->contact = new Contact();
    $this->contact->email = 'activecampaign_api@groowup.nl';
    $this->contact = $this->contactsEndpoint->create($this->contact);

    $this->tag = new TagContact();
    $this->tag->tag = 'activecampaign_api';
    $this->tag = $this->tagsEndpoint->create($this->tag);
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    $this->contactsEndpoint->delete($this->contact->id);
    $this->tagsEndpoint->delete($this->tag->id);
    parent::tearDown();
  }

  /**
   * TestCrud.
   */
  public function testCrud(): void {
    $this->assertCount(0, $this->endpoint->getAllByContact($this->contact));
    $this->assertNull($this->endpoint->getByContactTag($this->contact, $this->tag));

    $contact_tag = new ContactTag();
    $contact_tag->contact = $this->contact->id;
    $contact_tag->tag = $this->tag->id;
    $contact_tag = $this->endpoint->create($contact_tag);
    $this->assertInstanceOf(ContactTag::class, $contact_tag);
    $this->assertNotEmpty($contact_tag->id);

    $item = $this->endpoint->getByContactTag($this->contact, $this->tag);
    $this->assertInstanceOf(ContactTag::class, $item);
    $this->assertEquals($this->contact->id, $item->contact);
    $this->assertEquals($this->tag->id, $item->tag);

    $this->assertCount(1, $this->endpoint->getAllByContact($this->contact));

    $this->endpoint->delete($contact_tag->id);
    $this->assertCount(0, $this->endpoint->getAllByContact($this->contact));
    $this->assertNull($this->endpoint->getByContactTag($this->contact, $this->tag));
  }

}
