<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Account;
use Drupal\activecampaign_api\ApiResource\AccountContact;
use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\Endpoint\AccountContacts;
use Drupal\activecampaign_api\Endpoint\Accounts;
use Drupal\activecampaign_api\Endpoint\Contacts;

/**
 * Class AccountContactsTest.
 *
 * Tests the AccountContacts endpoint.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 *
 * @group activecampaign_api
 */
class AccountContactsTest extends ActivecampaignApiKernelTestBase {

  /**
   * The contacts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Contacts
   */
  protected $contactsEndpoint;

  /**
   * The accounts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Accounts
   */
  protected $accountsEndpoint;

  /**
   * The account contacts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\AccountContacts
   */
  protected $endpoint;

  /**
   * The contact.
   *
   * @var \Drupal\activecampaign_api\ApiResource\Contact
   */
  protected $contact;

  /**
   * The account.
   *
   * @var \Drupal\activecampaign_api\ApiResource\Account
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Contacts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Contacts::class);
    $this->contactsEndpoint = $endpoint;
    /**
     * @var \Drupal\activecampaign_api\Endpoint\Accounts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Accounts::class);
    $this->accountsEndpoint = $endpoint;

    /**
     * @var \Drupal\activecampaign_api\Endpoint\AccountContacts $endpoint
     */
    $endpoint = $this->endpointFactory->get(AccountContacts::class);
    $this->endpoint = $endpoint;

    $this->contact = new Contact();
    $this->contact->email = 'activecampaign_api@groowup.nl';
    $this->contact = $this->contactsEndpoint->create($this->contact);

    $this->account = new Account();
    $this->account->name = 'activecampaign_api';
    $this->account = $this->accountsEndpoint->create($this->account);
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    $this->contactsEndpoint->delete($this->contact->id);
    $this->accountsEndpoint->delete($this->account->id);
    parent::tearDown();
  }

  /**
   * TestCrud.
   */
  public function testCrud(): void {
    $this->assertCount(0, $this->endpoint->getAllByContact($this->contact));
    $this->assertCount(0, $this->endpoint->getAllByAccount($this->account));
    $this->assertNull($this->endpoint->getByAccountContact($this->account, $this->contact));

    $account_contact = new AccountContact();
    $account_contact->account = $this->account->id;
    $account_contact->contact = $this->contact->id;
    $account_contact->jobTitle = 'Job title';
    $account_contact = $this->endpoint->create($account_contact);
    $this->assertInstanceOf(AccountContact::class, $account_contact);
    $this->assertNotEmpty($account_contact->id);

    $this->assertCount(1, $this->endpoint->getAllByContact($this->contact));
    $this->assertCount(1, $this->endpoint->getAllByAccount($this->account));
    $account_contact_get = $this->endpoint->get($account_contact->id);
    $this->assertEquals($account_contact, $account_contact_get);

    $item = $this->endpoint->getByAccountContact($this->account, $this->contact);
    $this->assertInstanceOf(AccountContact::class, $item);
    $this->assertEquals($this->contact->id, $item->contact);
    $this->assertEquals($this->account->id, $item->account);

    $account_contact->jobTitle = 'Changed';
    $this->endpoint->update($account_contact);

    $account_contact_get = $this->endpoint->get($account_contact->id);
    $this->assertEquals($account_contact, $account_contact_get);

    $this->endpoint->delete($account_contact->id);
    $this->assertCount(0, $this->endpoint->getAllByContact($this->contact));
    $this->assertCount(0, $this->endpoint->getAllByAccount($this->account));
    $this->assertNull($this->endpoint->get($account_contact->id));
    $this->assertNull($this->endpoint->getByAccountContact($this->account, $this->contact));
  }

}
