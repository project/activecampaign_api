<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Field;
use Drupal\activecampaign_api\ApiResource\Field\Datetime;
use Drupal\activecampaign_api\Endpoint\Fields;

/**
 * Class ActiveCampaignApiServiceTest.
 *
 * Tests the ACtiveCampaignApiService class.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 * @group activecampaign_api
 */
class FieldsTest extends ActivecampaignApiKernelTestBase {

  /**
   * The fields endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Fields
   */
  protected $endpoint;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Fields $endpoint
     */
    $endpoint = $this->endpointFactory->get(Fields::class);
    $this->endpoint = $endpoint;
  }

  /**
   * TestFieldsCrud.
   */
  public function testFields(): void {
    $field = $this->endpoint->get(2);
    $this->assertInstanceOf(Datetime::class, $field);
    $this->assertEquals(2, $field->id);
    $this->assertEquals('DateTime field', $field->title);

    $fields = $this->endpoint->list([]);
    $this->assertIsArray($fields);
    foreach ($fields as $field) {
      $this->assertInstanceOf(Field::class, $field);

      $test = $this->endpoint->getByTitle($field->title);
      $this->assertEquals($field->id, $test->id);
    }
  }

}
