<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Account;
use Drupal\activecampaign_api\Endpoint\Accounts;
use Drupal\activecampaign_api\Exception;

/**
 * Class AccountsTest.
 *
 * Tests the Accounts endpoint.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 *
 * @group activecampaign_api
 */
class AccountsTest extends ActivecampaignApiKernelTestBase {

  /**
   * The accounts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Accounts
   */
  protected $endpoint;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Accounts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Accounts::class);
    $this->endpoint = $endpoint;
  }

  /**
   * TestAccountsCrud.
   */
  public function testAccountsCrud(): void {
    // Create.
    $new_account = new Account();
    $new_account->name = 'activecampaign_api';

    $this->assertCount(0, $this->endpoint->list(['name' => $new_account->name]));
    $account = $this->endpoint->create($new_account);

    $this->assertInstanceOf(Account::class, $account);

    $this->assertNotEmpty($account->id);
    $this->assertEquals($new_account->name, $account->name);
    $this->assertNotNull($this->endpoint->get($account->id));
    $list = $this->endpoint->list(['name' => $new_account->name]);
    $this->assertIsArray($list);
    $this->assertCount(1, $list);
    $this->assertEquals($new_account->name, $list[0]->name);

    // Update.
    $account->name = 'activecampaign_api2';
    $this->endpoint->update($account);
    $this->assertNotNull($this->endpoint->get($account->id));
    $updated_account = $this->endpoint->get($account->id);
    $this->assertEquals($account->name, $updated_account->name);

    // Delete.
    $this->endpoint->delete($account->id);

    $this->assertNull($this->endpoint->get($account->id));
    $this->assertCount(0, $this->endpoint->list(['name' => $account->name]));

    // Delete again should trigger a 404.
    try {
      $this->endpoint->delete($account->id);
      $this->fail('No exception thrown');
    }
    catch (Exception $e) {
      $this->assertEquals(404, $e->getResponse()->getStatusCode());
    }
    catch (\Exception $e) {
      $this->fail('Exception should be of type ' . Exception::class);
    }
  }

}
