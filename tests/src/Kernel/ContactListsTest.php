<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\ContactList;
use Drupal\activecampaign_api\Endpoint\ContactLists;

/**
 * Class ContactListsTest.
 *
 * Tests the ContactLists endpoint.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 * @group activecampaign_api
 */
class ContactListsTest extends ActivecampaignApiKernelTestBase {

  /**
   * The endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\ContactLists
   */
  protected $endpoint;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\ContactLists $endpoint
     */
    $endpoint = $this->endpointFactory->get(ContactLists::class);
    $this->endpoint = $endpoint;
  }

  /**
   * TestFields.
   */
  public function testGetList(): void {
    $contact_list = $this->endpoint->get(1);
    $this->assertInstanceOf(ContactList::class, $contact_list);
    $this->assertEquals(1, $contact_list->id);
    $this->assertEquals('Test list 1 Groowup', $contact_list->name);

    $fields = $this->endpoint->list([]);
    $this->assertIsArray($fields);
    foreach ($fields as $field) {
      $this->assertInstanceOf(ContactList::class, $field);

      $test = $this->endpoint->getByName($field->name);
      $this->assertEquals($field->id, $test->id);
    }
  }

}
