<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\Endpoint\Contacts;
use Drupal\activecampaign_api\Exception;

/**
 * Class ContactsTest.
 *
 * Tests the contacts endpoint.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 *
 * @group activecampaign_api
 */
class ContactsTest extends ActivecampaignApiKernelTestBase {

  /**
   * The contacts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Contacts
   */
  protected $endpoint;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Contacts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Contacts::class);
    $this->endpoint = $endpoint;
  }

  /**
   * TestContactsCrud.
   */
  public function testContactsCrud(): void {
    // Create.
    $new_contact = new Contact();
    $new_contact->email = 'activecampaign_api@groowup.nl';
    $new_contact->firstName = 'Firstname';
    $new_contact->lastName = 'Lastname';
    $new_contact->phone = '0768200222';

    $this->assertCount(0, $this->endpoint->list(['email' => $new_contact->email]));
    $contact = $this->endpoint->create($new_contact);

    $this->assertInstanceOf(Contact::class, $contact);

    $this->assertNotEmpty($contact->id);
    $this->assertEquals($new_contact->email, $contact->email);
    $this->assertEquals($new_contact->firstName, $contact->firstName);
    $this->assertEquals($new_contact->lastName, $contact->lastName);
    $this->assertEquals($new_contact->phone, $contact->phone);
    $this->assertCount(0, $contact->fieldValues);
    $this->assertNotNull($this->endpoint->get($contact->id));
    $list = $this->endpoint->list(['email' => $new_contact->email]);
    $this->assertIsArray($list);
    $this->assertCount(1, $list);
    $this->assertEquals($new_contact->email, $list[0]->email);

    // Create again should fail.
    try {
      $this->endpoint->create($new_contact);
      $this->fail('No exception thrown');
    }
    catch (Exception $e) {
      $this->assertEquals(422, $e->getResponse()->getStatusCode());
    }
    catch (\Exception $e) {
      $this->fail('Exception should be of type ' . Exception::class);
    }

    // Update.
    $contact->email = 'activecampaign_api2@groowup.nl';
    $contact->firstName = 'Changed';
    $this->endpoint->update($contact);
    $this->assertNotNull($this->endpoint->get($contact->id));
    $updated_contact = $this->endpoint->get($contact->id);
    $this->assertEquals($contact->email, $updated_contact->email);
    $this->assertEquals($contact->firstName, $updated_contact->firstName);
    $this->assertEquals($contact->lastName, $updated_contact->lastName);
    $this->assertCount(0, $contact->fieldValues);

    // Delete.
    $this->endpoint->delete($contact->id);

    $this->assertNull($this->endpoint->get($contact->id));
    $this->assertCount(0, $this->endpoint->list(['email' => $contact->email]));

    // Delete again should trigger a 404.
    try {
      $this->endpoint->delete($contact->id);
      $this->fail('No exception thrown');
    }
    catch (Exception $e) {
      $this->assertEquals(404, $e->getResponse()->getStatusCode());
    }
    catch (\Exception $e) {
      $this->fail('Exception should be of type ' . Exception::class);
    }
  }

}
