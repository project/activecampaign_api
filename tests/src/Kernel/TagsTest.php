<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Tag\Contact;
use Drupal\activecampaign_api\Endpoint\Tags;

/**
 * Class TagsTest.
 *
 * Tests the Tags class.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 * @group activecampaign_api
 */
class TagsTest extends ActivecampaignApiKernelTestBase {

  /**
   * The fields endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Tags
   */
  protected $endpoint;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Tags $endpoint
     */
    $endpoint = $this->endpointFactory->get(Tags::class);
    $this->endpoint = $endpoint;
  }

  /**
   * TestTags.
   */
  public function testTags(): void {
    $tag = $this->endpoint->get(5);
    $this->assertInstanceOf(Contact::class, $tag);
    $this->assertEquals(5, $tag->id);
    $this->assertEquals('Test tag 1', $tag->tag);

    $tags = $this->endpoint->list([]);
    $this->assertIsArray($tags);
    foreach ($tags as $tag) {
      $this->assertInstanceOf(Contact::class, $tag);
    }

    $new_tag = new Contact();
    $new_tag->tag = __FUNCTION__;

    $new_tag = $this->endpoint->create($new_tag);

    $this->assertInstanceOf(Contact::class, $new_tag);
    $this->assertNotEmpty($new_tag->id);
    $this->assertEquals(__FUNCTION__, $new_tag->tag);

    $tags = $this->endpoint->list(__FUNCTION__);
    $this->assertEquals(1, count($tags));
    $this->assertEquals($new_tag->id, $tags[0]->id);

    $new_tag->tag = __FUNCTION__ . '1';
    $this->endpoint->update($new_tag);
    $new_tag = $this->endpoint->get($new_tag->id);

    $this->assertInstanceOf(Contact::class, $new_tag);
    $this->assertEquals(__FUNCTION__ . '1', $new_tag->tag);

    $this->endpoint->delete($new_tag->id);

    $new_tag = $this->endpoint->get($new_tag->id);
    $this->assertNull($new_tag);
  }

}
