<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\Entity\ActivecampaignApiApiAccount;
use Drupal\KernelTests\KernelTestBase;

/**
 * Kernel test base class for activecampaign_api.
 */
abstract class ActivecampaignApiKernelTestBase extends KernelTestBase {

  /**
   * Test activecampaign api account.
   *
   * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface
   */
  protected $activecampaignApiAccount;

  /**
   * The endpoint factory.
   *
   * @var \Drupal\activecampaign_api\Service\EndpointFactoryInterface
   */
  protected $endpointFactory;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'activecampaign_api',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->activecampaignApiAccount = ActivecampaignApiApiAccount::create([
      'id' => 'activecampaign_api_test',
      'label' => 'activecampaign_api_test',
      'base_url' => $_ENV['ACTIVECAMPAIGN_API_TEST_BASE_URL'],
      'api_token' => $_ENV['ACTIVECAMPAIGN_API_TEST_API_TOKEN'],
      'event_tracking_base_url' => $_ENV['ACTIVECAMPAIGN_API_TEST_EVENT_TRACKING_BASE_URL'],
      'event_tracking_key' => $_ENV['ACTIVECAMPAIGN_API_TEST_EVENT_TRACKING_KEY'],
      'event_tracking_actid' => $_ENV['ACTIVECAMPAIGN_API_TEST_EVENT_TRACKING_ACTID'],
    ]);
    $this->activecampaignApiAccount->save();

    $this->endpointFactory = $this->container->get('activecampaign_api.endpoint_factory');
    $this->endpointFactory->setActivecampaignApiAccount($this->activecampaignApiAccount);
  }

}
