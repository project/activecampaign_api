<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta as AccountCustomFieldMetaResource;
use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta\Text;
use Drupal\activecampaign_api\Endpoint\AccountCustomFieldMeta;

/**
 * Class AccountCustomFieldMetaTest.
 *
 * Tests the AccountCustomFieldMeta endpoint.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 * @group activecampaign_api
 */
class AccountCustomFieldMetaTest extends ActivecampaignApiKernelTestBase {

  /**
   * The fields endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\AccountCustomFieldMeta
   */
  protected $endpoint;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\AccountCustomFieldMeta $endpoint
     */
    $endpoint = $this->endpointFactory->get(AccountCustomFieldMeta::class);
    $this->endpoint = $endpoint;
  }

  /**
   * TestFields.
   */
  public function testFields(): void {
    $field = $this->endpoint->get(2);
    $this->assertInstanceOf(Text::class, $field);
    $this->assertEquals(2, $field->id);
    $this->assertEquals('Address 1', $field->fieldLabel);

    $fields = $this->endpoint->list([]);
    $this->assertIsArray($fields);
    foreach ($fields as $field) {
      $this->assertInstanceOf(AccountCustomFieldMetaResource::class, $field);

      $test = $this->endpoint->getByFieldLabel($field->fieldLabel);
      $this->assertEquals($field->id, $test->id);
    }

    $this->assertNull($this->endpoint->getByFieldLabel('NONEXISTING'));
  }

}
