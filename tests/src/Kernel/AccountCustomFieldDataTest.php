<?php

namespace Drupal\Tests\activecampaign_api\Kernel;

use Drupal\activecampaign_api\ApiResource\Account;
use Drupal\activecampaign_api\ApiResource\AccountCustomFieldValue;
use Drupal\activecampaign_api\Endpoint\AccountCustomFieldData;
use Drupal\activecampaign_api\Endpoint\AccountCustomFieldMeta;
use Drupal\activecampaign_api\Endpoint\Accounts;

/**
 * Class AccountCustomFieldDataTest.
 *
 * Tests the AccountCustomFieldData endpoint.
 *
 * @package Drupal\Tests\activecampaign_api\Kernel
 *
 * @group activecampaign_api
 */
class AccountCustomFieldDataTest extends ActivecampaignApiKernelTestBase {

  /**
   * The accounts endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\Accounts
   */
  protected $accountsEndpoint;

  /**
   * The endpoint.
   *
   * @var \Drupal\activecampaign_api\Endpoint\AccountCustomFieldData
   */
  protected $endpoint;

  /**
   * Test account.
   *
   * @var \Drupal\activecampaign_api\ApiResource\Account
   */
  protected $account;

  /**
   * The custom fields by title.
   *
   * @var \Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta[]
   */
  protected $customFieldMetaByTitle = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Accounts $endpoint
     */
    $endpoint = $this->endpointFactory->get(Accounts::class);
    $this->accountsEndpoint = $endpoint;

    /**
     * @var \Drupal\activecampaign_api\Endpoint\AccountCustomFieldData $endpoint
     */
    $endpoint = $this->endpointFactory->get(AccountCustomFieldData::class);
    $this->endpoint = $endpoint;

    /**
     * @var \Drupal\activecampaign_api\Endpoint\AccountCustomFieldMeta $account_custom_field_meta_endpoint
     */
    $account_custom_field_meta_endpoint = $this->endpointFactory->get(AccountCustomFieldMeta::class);
    // @todo Replace 1000 with proper pagination support.
    foreach ($account_custom_field_meta_endpoint->list([], 0, 1000) as $field) {
      $this->customFieldMetaByTitle[$field->fieldLabel] = $field;
    }

    $this->account = new Account();
    $this->account->name = 'activecampaign_api';
    $this->account->fields[] = new AccountCustomFieldValue($this->customFieldMetaByTitle['Test field']->id, 99999999);
    $this->account = $this->accountsEndpoint->create($this->account);
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    $this->accountsEndpoint->delete($this->account->id);
    parent::tearDown();
  }

  /**
   * TestGetByAccount.
   */
  public function testGetByAccount(): void {
    $values = $this->endpoint->getByAccount($this->account);
    foreach ($values as $value) {
      $this->assertInstanceOf(AccountCustomFieldValue::class, $value);
    }
    $keys = array_keys($values);
    $this->assertCount(1, $values);
    $this->assertEquals($this->customFieldMetaByTitle['Test field']->id, $keys[0]);
    $this->assertEquals($this->customFieldMetaByTitle['Test field']->id, $values[$keys[0]]->customFieldId);
    $this->assertEquals(99999999, $values[$keys[0]]->fieldValue);
  }

  /**
   * TestGetByAccountField.
   */
  public function testGetByAccountField(): void {
    $values = $this->endpoint->getByAccount($this->account);
    $keys = array_keys($values);
    $field = $this->endpoint->getByAccountField($this->account, $this->customFieldMetaByTitle['Test field']->id);

    $this->assertInstanceOf(AccountCustomFieldValue::class, $field);
    $this->assertEquals($this->customFieldMetaByTitle['Test field']->id, $field->customFieldId);
    $this->assertEquals($values[$keys[0]]->fieldValue, $field->fieldValue);

    $field = $this->endpoint->getByAccountField($this->account, 'nonexisting');
    $this->assertNull($field);
  }

}
