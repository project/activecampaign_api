<?php

namespace Drupal\Tests\activecampaign_api\Unit;

use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\Tests\UnitTestCase;

/**
 * Class ApiResourceTest.
 *
 * Tests the ApiResource class.
 *
 * @package Drupal\Tests\activecampaign_api\Unit
 * @group activecampaign_api
 */
class ApiResourceTest extends UnitTestCase {

  /**
   * TestEquals.
   */
  public function testEquals(): void {
    $item1 = new Contact();
    $item1->id = '1';
    $item1->email = 'a';

    $item2 = new Contact();
    $item2->id = '1';
    $item2->email = 'a';

    $this->assertTrue($item1->equals($item2));
    $this->assertTrue($item2->equals($item1));

    $item2->email = 'b';
    $this->assertFalse($item1->equals($item2));
    $this->assertFalse($item2->equals($item1));

    unset($item2->email);
    $this->assertFalse($item1->equals($item2));
    $this->assertFalse($item2->equals($item1));

    $item2->email = $item1->email;
    $item1->firstName = 'test';
    $this->assertFalse($item1->equals($item2));
    $this->assertFalse($item2->equals($item1));
  }

}
