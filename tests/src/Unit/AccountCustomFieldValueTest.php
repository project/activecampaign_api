<?php

namespace Drupal\Tests\activecampaign_api\Unit;

use Drupal\activecampaign_api\ApiResource\AccountCustomFieldValue;
use Drupal\Tests\UnitTestCase;

/**
 * Class AccountCustomFieldValueTest.
 *
 * Tests the FieldValue class.
 *
 * @package Drupal\Tests\activecampaign_api\Unit
 * @group activecampaign_api
 */
class AccountCustomFieldValueTest extends UnitTestCase {

  /**
   * TestConstructor.
   */
  public function testConstructor(): void {
    $field_value = new AccountCustomFieldValue('1', '2');
    $this->assertEquals('1', $field_value->customFieldId);
    $this->assertEquals('2', $field_value->fieldValue);
  }

}
