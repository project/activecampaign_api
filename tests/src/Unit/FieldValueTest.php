<?php

namespace Drupal\Tests\activecampaign_api\Unit;

use Drupal\activecampaign_api\ApiResource\FieldValue;
use Drupal\Tests\UnitTestCase;

/**
 * Class FieldValueTest.
 *
 * Tests the FieldValue class.
 *
 * @package Drupal\Tests\activecampaign_api\Unit
 * @group activecampaign_api
 */
class FieldValueTest extends UnitTestCase {

  /**
   * TestConstructor.
   */
  public function testConstructor(): void {
    $field_value = new FieldValue('1', '2');
    $this->assertEquals('1', $field_value->field);
    $this->assertEquals('2', $field_value->value);
  }

}
