<?php

namespace Drupal\activecampaign_api;

use Psr\Http\Message\ResponseInterface;

/**
 * Class Exception.
 *
 * ActiveCampaign exception.
 *
 * @package Drupal\activecampaign_api
 */
class Exception extends \RuntimeException {

  /**
   * Is this exception reported?
   *
   * @var bool
   */
  protected $reported;

  /**
   * Construct the exception.
   *
   * @param string $message
   *   The Exception message to throw.
   * @param int $code
   *   The Exception code.
   * @param null|\Throwable $previous
   *   The previous throwable used for the exception chaining.
   * @param bool $reported
   *   TRUE if reported to
   *   endpoint_create_update_delete_error_reporting_webhook_url.
   */
  public function __construct($message = "", $code = 0, \Throwable $previous = NULL, bool $reported = FALSE) {
    parent::__construct($message, $code, $previous);
    $this->reported = $reported;
  }

  /**
   * Exception reported?
   *
   * @return bool
   *   TRUE if reported to
   *   endpoint_create_update_delete_error_reporting_webhook_url.
   */
  public function isReported(): bool {
    return $this->reported;
  }

  /**
   * Get the response.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The response.
   */
  public function getResponse(): ?ResponseInterface {
    $previous = $this->getPrevious();
    if ($previous && method_exists($previous, 'getResponse')) {
      return $previous->getResponse();
    }
    return NULL;
  }

}
