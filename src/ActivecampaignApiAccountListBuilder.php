<?php

namespace Drupal\activecampaign_api;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of activecampaign api account entities.
 */
class ActivecampaignApiAccountListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['base_url'] = $this->t('ActiveCampaign API base url');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
     * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $entity
     */
    $row['label'] = $entity->label();
    $row['base_url'] = $entity->getBaseUrl();
    return $row + parent::buildRow($entity);
  }

}
