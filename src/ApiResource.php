<?php

namespace Drupal\activecampaign_api;

/**
 * Class ApiResource.
 *
 * An ActiveCampaign API resource.
 *
 * @package Drupal\activecampaign_api
 */
abstract class ApiResource {

  /**
   * The resource ID.
   *
   * @var string
   */
  public $id;

  /**
   * Checks if the given resource equals this resource.
   *
   * @param \Drupal\activecampaign_api\ApiResource $resource
   *   The resource to compare with.
   *
   * @return bool
   *   TRUE if both are the same.
   */
  public function equals(ApiResource $resource) {
    foreach (get_object_vars($resource) as $property => $value) {
      if (!isset($this->$property) || $this->$property !== $value) {
        return FALSE;
      }
    }
    foreach (get_object_vars($this) as $property => $value) {
      if (!isset($resource->$property) && isset($this->$property)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Create a resource from a JSON response.
   *
   * @param object $json
   *   The JSON response.
   *
   * @return object
   *   The API resource created from the JSON string.
   */
  public static function createFromJsonResponse(object $json): object {
    $resource = new static();
    foreach (get_object_vars($resource) as $property => $value) {
      $resource->$property = $json->$property ?? NULL;
    }
    return $resource;
  }

  /**
   * Create a resource list from a JSON response list.
   *
   * @param array $list
   *   The JSON response list.
   *
   * @return static[]
   *   The API resources.
   */
  public static function createFromJsonListResponse(array $list): array {
    $resources = [];
    foreach ($list as $resource) {
      $resources[] = static::createFromJsonResponse($resource);
    }
    return $resources;
  }

}
