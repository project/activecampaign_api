<?php

namespace Drupal\activecampaign_api;

use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Endpoint.
 *
 * An endpoint.
 *
 * @package Drupal\activecampaign_api
 */
class Endpoint {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The activecampaign api account.
   *
   * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface
   */
  protected $activecampaignApiAccount;

  /**
   * The url.
   *
   * @var string
   */
  protected $url;

  /**
   * The resource name.
   *
   * @var string
   */
  protected $resource;

  /**
   * Endpoint constructor.
   *
   * @param string $resource
   *   The resource name.
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   */
  public function __construct(string $resource, Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    $this->httpClient = $httpClient;
    $this->moduleHandler = $moduleHandler;
    $this->activecampaignApiAccount = $activecampaignApiAccount;
    $this->url = $this->activecampaignApiAccount->getBaseUrl() . '/' . $resource;
    $this->resource = $resource;
  }

  /**
   * Set the Activecampaign api account to use.
   *
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The Activecampaign api account to use.
   */
  public function setActivecampaignApiAccount(ActivecampaignApiAccountInterface $activecampaignApiAccount): void {
    $this->activecampaignApiAccount = $activecampaignApiAccount;
    $this->url = $this->activecampaignApiAccount->getBaseUrl() . '/' . $this->resource;
  }

  /**
   * Get the resource name.
   *
   * @return string
   *   The resource name.
   */
  public function getResourceName(): string {
    return $this->resource;
  }

  /**
   * Get the API url.
   *
   * @return string
   *   The url.
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * Run a GET request.
   *
   * @param string $id
   *   The ID.
   *
   * @return object
   *   The response.
   */
  protected function getResource(string $id): ?object {
    if (empty($id)) {
      throw new \InvalidArgumentException('id is empty');
    }

    $url = $this->url . '/' . $id;
    $options = [
      'headers' => [
        'Api-Token' => $this->activecampaignApiAccount->getApiToken(),
      ],
    ];

    try {
      $response = $this->httpClient->get($url, $options);

      $contents = $response->getBody()->getContents();

      if (empty($contents)) {
        return NULL;
      }

      return json_decode($contents, FALSE, 512, JSON_THROW_ON_ERROR);
    }
    catch (ClientException $e) {
      if ($e->getResponse()->getStatusCode() === 404) {
        return NULL;
      }
      throw new Exception($e->getMessage(), $e->getCode(), $e);
    }
    catch (\Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * Get a resource.
   *
   * @param string $id
   *   The ID.
   *
   * @return object|null
   *   The resource or null if not found.
   */
  public function get(string $id) {
    return $this->getResource($id);
  }

  /**
   * Create a resource.
   *
   * @param object $data
   *   The data.
   *
   * @return object
   *   The results
   *
   * @throws \JsonException
   */
  protected function createResource(object $data): object {
    $this->moduleHandler->alter('activecampaign_api_endpoint_createresource', $data, $this);

    $options = [
      'headers' => [
        'Api-Token' => $this->activecampaignApiAccount->getApiToken(),
      ],
      'body' => json_encode($data),
    ];

    try {
      $response = $this->httpClient->post($this->url, $options);
    }
    catch (\Exception $e) {
      $is_reported = $this->reportErrorToWebhook($e, $this->url, 'POST', $options, $data);
      throw new Exception($e->getMessage(), $e->getCode(), $e, $is_reported);
    }

    $contents = $response->getBody()->getContents();

    return json_decode($contents, FALSE, 512, JSON_THROW_ON_ERROR);
  }

  /**
   * Update a resource.
   *
   * @param string $id
   *   The resource id.
   * @param object $data
   *   The data.
   */
  protected function updateResource(string $id, object $data): void {
    if (empty($id)) {
      throw new \InvalidArgumentException('id is empty');
    }

    $this->moduleHandler->alter('activecampaign_api_endpoint_updateresource', $data, $this);

    $url = $this->url . '/' . $id;

    $options = [
      'headers' => [
        'Api-Token' => $this->activecampaignApiAccount->getApiToken(),
      ],
      'body' => json_encode($data),
    ];

    try {
      $response = $this->httpClient->put($url, $options);
    }
    catch (\Exception $e) {
      $is_reported = $this->reportErrorToWebhook($e, $url, 'PUT', $options, $data);
      throw new Exception($e->getMessage(), $e->getCode(), $e, $is_reported);
    }

    if ($response->getStatusCode() !== 200) {
      throw new Exception('Status code !== 200');
    }
  }

  /**
   * Delete a resource.
   *
   * @param string $id
   *   The resource id.
   */
  protected function deleteResource(string $id): void {
    if (empty($id)) {
      throw new \InvalidArgumentException('id is empty');
    }

    $url = $this->url . '/' . $id;
    $options = [
      'headers' => [
        'Api-Token' => $this->activecampaignApiAccount->getApiToken(),
      ],
    ];

    try {
      $this->httpClient->delete($url, $options);
    }
    catch (\Exception $e) {
      $is_reported = $this->reportErrorToWebhook($e, $url, 'DELETE', $options);
      throw new Exception($e->getMessage(), $e->getCode(), $e, $is_reported);
    }
  }

  /**
   * Delete the resource.
   *
   * @param string $id
   *   The resource ID.
   */
  public function delete(string $id): void {
    $this->deleteResource($id);
  }

  /**
   * List resources.
   *
   * @param array $filters
   *   The filters to apply.
   * @param int $offset
   *   The pagination offset.
   * @param int|null $limit
   *   The pagination limit or null to get all.
   *
   * @return object
   *   The results
   */
  protected function listResources(array $filters, int $offset = 0, int $limit = NULL): object {

    $query = $filters;
    $query['offset'] = $offset;
    $query['limit'] = $limit ?? 20;

    $url = $this->url;
    $options = [
      'headers' => [
        'Api-Token' => $this->activecampaignApiAccount->getApiToken(),
      ],
      'query' => $query,
    ];

    $results = NULL;
    $expected_results = NULL;

    try {
      do {
        $response = $this->httpClient->get($url, $options);

        $contents = $response->getBody()->getContents();
        $json = json_decode($contents, FALSE, 512, JSON_THROW_ON_ERROR);

        if (!isset($json->meta->total)) {
          throw new \RuntimeException('meta.total is missing');
        }

        if ($results === NULL) {
          $results = $json;
          $expected_results = $limit - $offset;
          if ($limit === NULL || $expected_results > $json->meta->total - $offset) {
            $expected_results = $json->meta->total - $offset;
          }
        }
        else {
          $results->{$this->resource} = array_merge($results->{$this->resource}, $json->{$this->resource});
        }

        $options['query']['offset'] += count($json->{$this->resource});

      } while ($expected_results > count($results->{$this->resource}));

      return $results;
    }
    catch (\Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * Post data to an endpoint.
   *
   * @param string $endpoint
   *   The relative endpoint url.
   * @param object $data
   *   The data to post.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  protected function post(string $endpoint, object $data): ResponseInterface {
    $options = [
      'headers' => [
        'Api-Token' => $this->activecampaignApiAccount->getApiToken(),
      ],
      'body' => json_encode($data),
    ];

    $url = $this->activecampaignApiAccount->getBaseUrl() . $endpoint;

    try {
      return $this->httpClient->post($url, $options);
    }
    catch (\Exception $e) {
      $is_reported = $this->reportErrorToWebhook($e, $url, 'post', $options, $data);
      throw new Exception($e->getMessage(), $e->getCode(), $e, $is_reported);
    }
  }

  /**
   * Report the exception to the reporting webhook url.
   *
   * @param \Exception $e
   *   The exception.
   * @param string $url
   *   The requested url.
   * @param string $method
   *   The request method.
   * @param array $options
   *   The request options.
   * @param object|null $data
   *   The data sent with the request.
   *
   * @return bool
   *   TRUE when reported, FALSE when not.
   */
  protected function reportErrorToWebhook(\Exception $e, string $url, string $method, array $options, object $data = NULL): bool {
    $error_reporting_webhook_url = $this->activecampaignApiAccount->getEndpointCreateUpdateDeleteErrorReportingWebhookUrl();
    if (empty($error_reporting_webhook_url)) {
      return FALSE;
    }

    $response = NULL;
    if ($e instanceof ClientException) {
      $response = $e->getResponse()->getBody()->getContents();
    }

    if ($data === NULL) {
      $data = new \stdClass();
    }

    if (isset($options['headers']['Api-Token'])) {
      $options['headers']['Api-Token'] = '--hidden--';
    }

    $data->activecampaign_api_account = $this->activecampaignApiAccount->id();
    $data->api_call_url = $url;
    $data->api_call_options = $options;
    $data->api_call_method = $method;
    $data->api_call_error = $e->getMessage();
    $data->api_call_response = empty($response) ? '--empty response--' : $response;

    $this->moduleHandler->alter('activecampaign_api_report_error_to_webhook', $data, $this);

    try {
      $response = $this->httpClient->post($error_reporting_webhook_url, [
        'form_params' => get_object_vars($data),
      ]);
    }
    catch (\Exception $reporting_exception) {
      watchdog_exception('activecampaign_api', $reporting_exception);
      return FALSE;
    }

    return TRUE;
  }

}
