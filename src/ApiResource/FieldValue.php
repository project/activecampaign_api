<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class FieldValue.
 *
 * The FieldValue resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class FieldValue extends ApiResource implements \JsonSerializable {

  /**
   * The contact this field value belongs to.
   *
   * @var string|null
   */
  public $contact;

  /**
   * The field id.
   *
   * @var string
   */
  public $field;

  /**
   * The value.
   *
   * @var mixed
   */
  public $value;

  /**
   * FieldValue constructor.
   *
   * @param string|null $field
   *   The field id.
   * @param mixed|null $value
   *   The value.
   * @param string|null $contact
   *   The contact.
   */
  public function __construct(string $field = NULL, $value = NULL, string $contact = NULL) {
    $this->field = $field;
    $this->value = $value;
    $this->contact = $contact;
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): mixed {
    $object = new \stdClass();
    if (!empty($this->id)) {
      $object->id = $this->id;
    }
    $object->field = $this->field;
    $object->value = $this->value;
    if (!empty($this->contact)) {
      $object->contact = $this->contact;
    }

    return $object;
  }

}
