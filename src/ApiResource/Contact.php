<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class Contact.
 *
 * The contact resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class Contact extends ApiResource {

  /**
   * Email address.
   *
   * @var string
   */
  public $email = '';

  /**
   * The given name.
   *
   * @var string
   */
  public $firstName = '';

  /**
   * The family name.
   *
   * @var string
   */
  public $lastName = '';

  /**
   * The telephone number.
   *
   * @var string
   */
  public $phone = '';

  /**
   * Field values or null if not loaded.
   *
   * @var \Drupal\activecampaign_api\ApiResource\FieldValue[]
   */
  public $fieldValues = [];

  /**
   * {@inheritdoc}
   */
  public static function createFromJsonResponse(object $json): object {
    $contact = parent::createFromJsonResponse($json);

    // fieldValues contains value ids and not real FieldValue instances.
    $contact->fieldValues = [];

    return $contact;
  }

}
