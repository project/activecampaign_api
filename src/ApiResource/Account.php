<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class Account.
 *
 * The account resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class Account extends ApiResource {

  /**
   * Name.
   *
   * @var string
   */
  public $name = '';

  /**
   * Custom field values.
   *
   * @var \Drupal\activecampaign_api\ApiResource\AccountCustomFieldValue[]
   */
  public $fields = [];

  /**
   * {@inheritdoc}
   */
  public static function createFromJsonResponse(object $json): object {
    $contact = parent::createFromJsonResponse($json);

    // Fields are not returned from the api.
    $contact->fields = [];

    return $contact;
  }

}
