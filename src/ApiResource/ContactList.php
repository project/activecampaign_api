<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class ContactList.
 *
 * The list resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class ContactList extends ApiResource {

  /**
   * Name.
   *
   * @var string
   */
  public $name = '';

  /**
   * The system name.
   *
   * @var string
   */
  public $stringid;

}
