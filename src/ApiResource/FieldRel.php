<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class FieldRel.
 *
 * The fieldrel resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class FieldRel extends ApiResource {

  /**
   * The field id.
   *
   * @var string
   */
  public $field;

  /**
   * The related list id.
   *
   * @var string
   */
  public $relid;

}
