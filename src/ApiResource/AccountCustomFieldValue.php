<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class AccountCustomFieldValue.
 *
 * The AccountCustomFieldValue resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class AccountCustomFieldValue extends ApiResource implements \JsonSerializable {

  /**
   * The field id.
   *
   * @var string
   */
  public $customFieldId;

  /**
   * The value.
   *
   * @var mixed
   */
  public $fieldValue;

  /**
   * AccountCustomFieldValue constructor.
   *
   * @param string $customFieldId
   *   The field id.
   * @param mixed $fieldValue
   *   The value.
   */
  public function __construct(string $customFieldId, $fieldValue) {
    $this->customFieldId = $customFieldId;
    $this->fieldValue = $fieldValue;
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): mixed {
    $object = new \stdClass();
    if (!empty($this->id)) {
      $object->id = $this->id;
    }
    $object->customFieldId = $this->customFieldId;
    $object->fieldValue = $this->fieldValue;

    return $object;
  }

}
