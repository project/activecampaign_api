<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class AccountContact.
 *
 * The AccountContact resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class AccountContact extends ApiResource {

  /**
   * The account id.
   *
   * @var string
   */
  public $account;

  /**
   * The contact id.
   *
   * @var string
   */
  public $contact;

  /**
   * Job Title of the contact at the account.
   *
   * @var string
   */
  public $jobTitle;

}
