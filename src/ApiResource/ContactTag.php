<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class ContactTag.
 *
 * The ContactTag resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class ContactTag extends ApiResource {

  /**
   * The contact id.
   *
   * @var string
   */
  public $contact;

  /**
   * The tag id.
   *
   * @var string
   */
  public $tag;

}
