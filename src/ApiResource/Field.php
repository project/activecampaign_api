<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class Field.
 *
 * The field resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
abstract class Field extends ApiResource {

  /**
   * The field title.
   *
   * @var string
   */
  public $title;

  /**
   * Get the typed instance for the given JSON response.
   *
   * @param object $json
   *   The JSON response.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Field
   *   The typed instance, a subclass of Field.
   */
  public static function getTypedInstance(object $json): Field {
    $class_name = __NAMESPACE__ . '\\Field\\' . ucfirst($json->type);

    if (!class_exists($class_name, TRUE)) {
      throw new \RuntimeException('Class ' . $class_name . ' does not exist.');
    }

    return call_user_func([$class_name, 'createFromJsonResponse'], $json);
  }

}
