<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class Tag.
 *
 * The tag resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
abstract class Tag extends ApiResource {

  /**
   * Name of the tag.
   *
   * @var string
   */
  public $tag;

  /**
   * The tag type.
   *
   * @var string
   */
  public $tagType;

  /**
   * Description of the tag.
   *
   * @var string
   */
  public $description;

  /**
   * Get the typed instance for the given JSON response.
   *
   * @param object $json
   *   The JSON response.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Tag
   *   The typed instance, a subclass of Tag.
   */
  public static function getTypedInstance(object $json): Tag {
    $class_name = __NAMESPACE__ . '\\Tag\\' . ucfirst($json->tagType);

    if (!class_exists($class_name, TRUE)) {
      throw new \RuntimeException('Class ' . $class_name . ' does not exist.');
    }

    /**
     * @var \Drupal\activecampaign_api\ApiResource\Tag $instance
     */
    $instance = call_user_func([$class_name, 'createFromJsonResponse'], $json);
    $instance->tagType = self::getTagTypeForClass($instance);
    return $instance;
  }

  /**
   * Get the AC tagType value for a tag instance.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Tag $tag
   *   The tag.
   *
   * @return string
   *   The AC tagType.
   */
  public static function getTagTypeForClass(Tag $tag) {
    $tag_type = explode('\\', get_class($tag));
    $tag_type = $tag_type[count($tag_type) - 1];
    return lcfirst($tag_type);
  }

}
