<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

use Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class FieldOptions.
 *
 * The FieldOptions abstract field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
abstract class FieldOptions extends Field {
  /**
   * The available options.
   *
   * @var string[]
   */
  public $fieldOptions = [];

}
