<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

use Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Listbox.
 *
 * The Listbox field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Listbox extends Field {

}
