<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

use Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Text.
 *
 * The text field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Text extends Field {

}
