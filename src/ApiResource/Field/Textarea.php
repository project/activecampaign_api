<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

use Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Textarea.
 *
 * The Textarea field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Textarea extends Field {

}
