<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Dropdown.
 *
 * The Dropdown field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Dropdown extends FieldOptions {

}
