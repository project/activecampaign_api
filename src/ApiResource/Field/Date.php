<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

use Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Date.
 *
 * The Date field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Date extends Field {

}
