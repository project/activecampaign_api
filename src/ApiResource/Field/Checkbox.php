<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Checkbox.
 *
 * The Checkbox field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Checkbox extends FieldOptions {

}
