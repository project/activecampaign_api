<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

use Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Number.
 *
 * The number field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Number extends Field {

}
