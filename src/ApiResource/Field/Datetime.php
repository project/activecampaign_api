<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

use Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Datetime.
 *
 * The datetime field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Datetime extends Field {

  const FORMAT = \DateTimeInterface::W3C;

}
