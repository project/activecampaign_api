<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

use Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Hidden.
 *
 * The Hidden field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Hidden extends Field {

}
