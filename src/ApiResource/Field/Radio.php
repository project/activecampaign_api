<?php

namespace Drupal\activecampaign_api\ApiResource\Field;

/**
 * Class Radio.
 *
 * The Radio field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Field
 */
class Radio extends FieldOptions {

}
