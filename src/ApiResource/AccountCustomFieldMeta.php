<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class AccountCustomFieldMeta.
 *
 * The AccountCustomFieldMeta resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
abstract class AccountCustomFieldMeta extends ApiResource {

  /**
   * The field title.
   *
   * @var string
   */
  public $fieldLabel;

  /**
   * Get the typed instance for the given JSON response.
   *
   * @param object $json
   *   The JSON response.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
   *   The typed instance, a subclass of Field.
   */
  public static function getTypedInstance(object $json): AccountCustomFieldMeta {
    $class_name = __NAMESPACE__ . '\\AccountCustomFieldMeta\\' . ucfirst($json->fieldType);

    if (!class_exists($class_name, TRUE)) {
      throw new \RuntimeException('Class ' . $class_name . ' does not exist.');
    }

    return call_user_func([$class_name, 'createFromJsonResponse'], $json);
  }

}
