<?php

namespace Drupal\activecampaign_api\ApiResource;

use Drupal\activecampaign_api\ApiResource;

/**
 * Class ContactListMembership.
 *
 * The contact list membership resource.
 *
 * @package Drupal\activecampaign_api\ApiResource
 */
class ContactListMembership extends ApiResource {

  /**
   * The contact id.
   *
   * @var string
   */
  public $contact = '';

  /**
   * The list id.
   *
   * @var string
   */
  public $list = '';

  /**
   * The status.
   *
   * @var string
   */
  public $status = '';

}
