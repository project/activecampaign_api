<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class Radio.
 *
 * The Radio field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
class Radio extends FieldOptions {

}
