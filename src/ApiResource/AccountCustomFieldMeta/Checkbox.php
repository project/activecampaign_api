<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class Checkbox.
 *
 * The Checkbox field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
class Checkbox extends FieldOptions {

}
