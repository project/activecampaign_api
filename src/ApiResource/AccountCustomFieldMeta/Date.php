<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class Date.
 *
 * The Date field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
class Date extends AccountCustomFieldMeta {

}
