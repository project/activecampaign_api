<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class Text.
 *
 * The text field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
class Text extends AccountCustomFieldMeta {

}
