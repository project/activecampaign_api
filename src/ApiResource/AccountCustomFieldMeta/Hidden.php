<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class Hidden.
 *
 * The Hidden field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
class Hidden extends AccountCustomFieldMeta {

}
