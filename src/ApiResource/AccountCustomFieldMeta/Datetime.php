<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class Datetime.
 *
 * The datetime field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
class Datetime extends AccountCustomFieldMeta {

  const FORMAT = \DateTimeInterface::W3C;

}
