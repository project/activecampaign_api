<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class Textarea.
 *
 * The Textarea field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
class Textarea extends AccountCustomFieldMeta {

}
