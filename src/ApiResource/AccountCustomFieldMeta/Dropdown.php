<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class Dropdown.
 *
 * The Dropdown field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
class Dropdown extends FieldOptions {

}
