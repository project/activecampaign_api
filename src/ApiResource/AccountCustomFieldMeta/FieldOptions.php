<?php

namespace Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta;

/**
 * Class FieldOptions.
 *
 * The FieldOptions abstract field type.
 *
 * @package Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta
 */
abstract class FieldOptions extends AccountCustomFieldMeta {
  /**
   * The available options.
   *
   * @var string[]
   */
  public $fieldOptions = [];

}
