<?php

namespace Drupal\activecampaign_api\ApiResource\Tag;

use Drupal\activecampaign_api\ApiResource\Tag;

/**
 * Class Template.
 *
 * The Template tag type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Tag
 */
class Template extends Tag {

}
