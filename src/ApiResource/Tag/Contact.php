<?php

namespace Drupal\activecampaign_api\ApiResource\Tag;

use Drupal\activecampaign_api\ApiResource\Tag;

/**
 * Class Contact.
 *
 * The Contact tag type.
 *
 * @package Drupal\activecampaign_api\ApiResource\Tag
 */
class Contact extends Tag {

}
