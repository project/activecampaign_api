<?php

namespace Drupal\activecampaign_api;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for the activecampaign api account entity.
 */
interface ActivecampaignApiAccountInterface extends ConfigEntityInterface {

  /**
   * Get the ActiveCampaign API base url.
   *
   * @return string
   *   The ActiveCampaign API base url
   */
  public function getBaseUrl(): ?string;

  /**
   * Get the ActiveCampaign API token.
   *
   * @return string
   *   The ActiveCampaign API token
   */
  public function getApiToken(): ?string;

  /**
   * Get the event tracking base url.
   *
   * @return string
   *   The event tracking base url
   */
  public function getEventTrackingBaseUrl(): ?string;

  /**
   * Get the event tracking key.
   *
   * @return string
   *   The event tracking key
   */
  public function getEventTrackingKey(): ?string;

  /**
   * Get the event tracking actid.
   *
   * @return string
   *   The event tracking actid
   */
  public function getEventTrackingActId(): ?string;

  /**
   * Get the URL to report (POST) create/update/delete errors to.
   *
   * @return string
   *   The URL
   */
  public function getEndpointCreateUpdateDeleteErrorReportingWebhookUrl(): ?string;

}
