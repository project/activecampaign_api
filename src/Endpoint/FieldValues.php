<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\ApiResource\Field;
use Drupal\activecampaign_api\ApiResource\FieldValue;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class FieldValues.
 *
 * The FieldValues endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class FieldValues extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('fieldValues', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * Get a field value.
   *
   * @param string $id
   *   The ID.
   *
   * @return \Drupal\activecampaign_api\ApiResource\FieldValue|null
   *   The field value or null if not found
   */
  public function get(string $id): ?FieldValue {
    $json = $this->getResource($id);

    if ($json === NULL) {
      return NULL;
    }

    return FieldValue::createFromJsonResponse($json->contact);
  }

  /**
   * Create a field value.
   *
   * @param \Drupal\activecampaign_api\ApiResource\FieldValue $fieldValue
   *   The field value to create.
   *
   * @return \Drupal\activecampaign_api\ApiResource\FieldValue
   *   The new field value.
   *
   * @throws \JsonException
   */
  public function create(FieldValue $fieldValue): FieldValue {
    if (empty($fieldValue->contact)) {
      throw new \InvalidArgumentException('Contact is empty');
    }

    $data = new \stdClass();
    $data->fieldValue = clone $fieldValue;

    unset($data->fieldValue->id);

    $json = $this->createResource($data);
    return FieldValue::createFromJsonResponse($json->fieldValue);
  }

  /**
   * Update a field value.
   *
   * @param \Drupal\activecampaign_api\ApiResource\FieldValue $fieldValue
   *   The field value to update.
   */
  public function update(FieldValue $fieldValue): void {
    if (empty($fieldValue->id)) {
      throw new \InvalidArgumentException('ID is empty');
    }
    if (empty($fieldValue->contact)) {
      throw new \InvalidArgumentException('Contact is empty');
    }
    $data = new \stdClass();
    $data->fieldValue = clone $fieldValue;

    $this->updateResource($data->fieldValue->id, $data);
  }

  /**
   * Get a field value for a contact.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact.
   * @param \Drupal\activecampaign_api\ApiResource\Field $field
   *   The field.
   *
   * @return \Drupal\activecampaign_api\ApiResource\FieldValue|null
   *   The field value or null if not found.
   */
  public function getByContactField(Contact $contact, Field $field): ?FieldValue {
    $list_filters = [
      'filters[fieldid]' => $field->id,
    ];

    // @todo fix proper pagination.
    $json = $this->listResources($list_filters, 0, 10000);

    $field_values = FieldValue::createFromJsonListResponse($json->fieldValues);

    // API doesn't support filtering by contact, so we do it manually.
    foreach ($field_values as $field_value) {
      if ($field_value->contact == $contact->id) {
        return $field_value;
      }
    }

    return NULL;
  }

}
