<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\ApiResource\ContactList;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class ContactLists.
 *
 * The lists endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class ContactLists extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('lists', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * Get a list.
   *
   * @param string $id
   *   The ID.
   *
   * @return \Drupal\activecampaign_api\ApiResource\ContactList|null
   *   The list or null if not found
   */
  public function get(string $id): ?ContactList {
    $json = $this->getResource($id);

    if ($json === NULL) {
      return NULL;
    }

    return ContactList::createFromJsonResponse($json->list);
  }

  /**
   * Get a list by name.
   *
   * @param string $name
   *   The name.
   *
   * @return \Drupal\activecampaign_api\ApiResource\ContactList|null
   *   The list or null if not found
   */
  public function getByName(string $name): ?ContactList {
    static $cache = NULL;

    if ($cache === NULL) {
      $cache = [];
      foreach ($this->list([], 0, NULL) as $item) {
        $cache[$item->name] = $item;
      }
    }

    return $cache[$name] ?? NULL;
  }

  /**
   * List lists.
   *
   * @param array $filters
   *   Filters to apply.
   * @param int $offset
   *   The pagination offset.
   * @param int|null $limit
   *   The pagination limit or null to get all.
   *
   * @return \Drupal\activecampaign_api\ApiResource\ContactList[]
   *   The list of lists.
   */
  public function list(array $filters, int $offset = 0, int $limit = NULL): array {
    $list_filters = [];

    foreach ($filters as $key => $value) {
      $list_filters['filters[' . $key . ']'] = $value;
    }

    $json = $this->listResources($list_filters, $offset, $limit);

    return ContactList::createFromJsonListResponse($json->lists);
  }

  /**
   * Update list subscription status for a contact.
   *
   * @param \Drupal\activecampaign_api\ApiResource\ContactList $list
   *   The list.
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact.
   * @param bool $subscribe
   *   Subscribe? Set to false to unsubscribe.
   */
  public function updateStatus(ContactList $list, Contact $contact, bool $subscribe): void {
    $data = new \stdClass();
    $data->contactList = new \stdClass();
    $data->contactList->list = $list->id;
    $data->contactList->contact = $contact->id;
    $data->contactList->status = $subscribe === TRUE ? '1' : '2';

    $this->post('/contactLists', $data);
  }

}
