<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\ApiResource\ContactTag;
use Drupal\activecampaign_api\ApiResource\Tag;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class ContactTags.
 *
 * The ContactTags endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class ContactTags extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('contactTags', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $id) {
    throw new \RuntimeException('Not implemented');
  }

  /**
   * Create a account.
   *
   * @param \Drupal\activecampaign_api\ApiResource\ContactTag $contactTag
   *   The account contact to create.
   *
   * @return \Drupal\activecampaign_api\ApiResource\ContactTag
   *   The new account contact.
   *
   * @throws \JsonException
   */
  public function create(ContactTag $contactTag): ContactTag {
    $data = new \stdClass();
    $data->contactTag = clone $contactTag;

    unset($data->contactTag->id);

    $json = $this->createResource($data);
    return ContactTag::createFromJsonResponse($json->contactTag);
  }

  /**
   * Get a contactTag.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact.
   * @param \Drupal\activecampaign_api\ApiResource\Tag $tag
   *   The tag.
   *
   * @return \Drupal\activecampaign_api\ApiResource\ContactTag|null
   *   The contactTag instance or null if not found.
   */
  public function getByContactTag(Contact $contact, Tag $tag): ?ContactTag {
    $all_contact_tags = $this->getAllByContact($contact);

    // The contactTags api doesn't support filtering.
    $contact_tags = [];
    foreach ($all_contact_tags as $contact_tag) {
      if ($contact_tag->tag == $tag->id) {
        $contact_tags[] = $contact_tag;
      }
    }

    if (count($contact_tags) > 1) {
      throw new \RuntimeException('Count > 1');
    }
    if (count($contact_tags) === 0) {
      return NULL;
    }
    return $contact_tags[0];
  }

  /**
   * Get all by contact.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact.
   *
   * @return \Drupal\activecampaign_api\ApiResource\ContactTag[]
   *   The list of contact tags.
   */
  public function getAllByContact(Contact $contact): array {
    $url = $this->activecampaignApiAccount->getBaseUrl() . '/contacts/' . $contact->id . '/' . $this->resource;

    $options = [
      'headers' => [
        'Api-Token' => $this->activecampaignApiAccount->getApiToken(),
      ],
    ];

    try {
      $response = $this->httpClient->get($url, $options);

      $contents = $response->getBody()->getContents();

      $json = json_decode($contents, FALSE, 512, JSON_THROW_ON_ERROR);

      return ContactTag::createFromJsonListResponse($json->contactTags);
    }
    catch (\Exception $e) {
      throw new \Exception($e->getMessage(), $e->getCode(), $e);
    }
  }

}
