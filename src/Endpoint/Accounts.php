<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Account;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class Accounts.
 *
 * The Accounts endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class Accounts extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('accounts', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * Get a account.
   *
   * @param string $id
   *   The ID.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Account|null
   *   The account or null if not found
   */
  public function get(string $id): ?Account {
    $json = $this->getResource($id);

    if ($json === NULL) {
      return NULL;
    }

    return Account::createFromJsonResponse($json->account);
  }

  /**
   * Create a account.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Account $account
   *   The account to create.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Account
   *   The new account.
   *
   * @throws \JsonException
   */
  public function create(Account $account): Account {
    $data = new \stdClass();
    $data->account = clone $account;

    unset($data->account->id);

    $json = $this->createResource($data);
    return Account::createFromJsonResponse($json->account);
  }

  /**
   * Update a account.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Account $account
   *   The account to update.
   */
  public function update(Account $account): void {
    if (empty($account->id)) {
      throw new \InvalidArgumentException('ID is empty');
    }
    $data = new \stdClass();
    $data->account = clone $account;

    $this->updateResource($data->account->id, $data);
  }

  /**
   * List accounts.
   *
   * @param array $filters
   *   Filters to apply.
   * @param int $offset
   *   The pagination offset.
   * @param int|null $limit
   *   The pagination limit or null to get all.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Account[]
   *   The list of accounts.
   */
  public function list(array $filters, int $offset = 0, int $limit = NULL): array {
    $list_filters = [];

    foreach ($filters as $key => $value) {
      $list_filters['filters[' . $key . ']'] = $value;
    }

    $json = $this->listResources($list_filters, $offset, $limit);

    return Account::createFromJsonListResponse($json->accounts);
  }

}
