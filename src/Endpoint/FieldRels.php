<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\FieldRel;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class FieldRels.
 *
 * The FieldRels endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class FieldRels extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('fieldRels', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * List fieldrels.
   *
   * @param array $filters
   *   Filters to apply.
   * @param int $offset
   *   The pagination offset.
   * @param int|null $limit
   *   The pagination limit or null to get all.
   *
   * @return \Drupal\activecampaign_api\ApiResource\FieldRel[]
   *   The list of fieldrels.
   */
  public function list(array $filters, int $offset = 0, int $limit = NULL): array {
    $list_filters = [];

    foreach ($filters as $key => $value) {
      $list_filters['filters[' . $key . ']'] = $value;
    }

    $json = $this->listResources($list_filters, $offset, $limit);

    return FieldRel::createFromJsonListResponse($json->fieldRels);
  }

}
