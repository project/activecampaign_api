<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Account;
use Drupal\activecampaign_api\ApiResource\AccountCustomFieldValue;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class AccountCustomFieldData.
 *
 * The AccountCustomFieldData endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class AccountCustomFieldData extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('accountCustomFieldData', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * List values.
   *
   * @param array $filters
   *   Filters to apply.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountCustomFieldValue[]
   *   The list of values.
   */
  protected function list(array $filters): array {
    $list_filters = [];

    foreach ($filters as $key => $value) {
      $list_filters['filters[' . $key . ']'] = $value;
    }

    $json = $this->listResources($list_filters);

    $values = [];
    foreach ($json->accountCustomFieldData as $item) {
      $values[$item->customFieldId] = new AccountCustomFieldValue(
        $item->customFieldId,
        $item->fieldValue
      );
    }
    return $values;
  }

  /**
   * Get values for the given account.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Account $account
   *   The account.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountCustomFieldValue[]
   *   The values.
   */
  public function getByAccount(Account $account): array {
    return $this->list(['customerAccountId' => $account->id]);
  }

  /**
   * Get a field value.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Account $account
   *   The account.
   * @param string $customFieldId
   *   The field id.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountCustomFieldValue|null
   *   The value or null if not found.
   */
  public function getByAccountField(Account $account, string $customFieldId): ?AccountCustomFieldValue {
    foreach ($this->getByAccount($account) as $accountCustomFieldValue) {
      if ($accountCustomFieldValue->customFieldId == $customFieldId) {
        return $accountCustomFieldValue;
      }
    }
    return NULL;
  }

}
