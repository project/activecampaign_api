<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Field;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class Fields.
 *
 * The Fields endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class Fields extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('fields', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * Get a field.
   *
   * @param string $id
   *   The ID.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Field|null
   *   The field or null if not found
   */
  public function get(string $id): ?Field {
    $json = $this->getResource($id);

    if ($json === NULL) {
      return NULL;
    }

    return Field::getTypedInstance($json->field);
  }

  /**
   * Get a field by its title.
   *
   * @param string $title
   *   The title.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Field|null
   *   The field or null if not found
   */
  public function getByTitle(string $title): ?Field {
    static $cache = NULL;
    if ($cache === NULL) {
      $cache = [];
      foreach ($this->list([], 0, NULL) as $item) {
        $cache[$item->title] = $item;
      }
    }

    return $cache[$title] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $id): void {
    throw new \RuntimeException('Not implemented');
  }

  /**
   * List fields.
   *
   * @param array $filters
   *   Filters to apply.
   * @param int $offset
   *   The pagination offset.
   * @param int|null $limit
   *   The pagination limit or null to get all.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Field[]
   *   The list of fields.
   */
  public function list(array $filters, int $offset = 0, int $limit = NULL): array {
    $list_filters = [];

    foreach ($filters as $key => $value) {
      $list_filters['filters[' . $key . ']'] = $value;
    }

    $json = $this->listResources($list_filters, $offset, $limit);

    $fields = [];
    foreach ($json->fields as $field) {
      $fields[] = Field::getTypedInstance($field);
    }
    return $fields;
  }

}
