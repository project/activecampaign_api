<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\ApiResource\ContactListMembership;
use Drupal\activecampaign_api\Endpoint;
use Drupal\activecampaign_api\Exception;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class Contacts.
 *
 * The Contacts endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class Contacts extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('contacts', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * Get a contact.
   *
   * @param string $id
   *   The ID.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Contact|null
   *   The contact or null if not found
   */
  public function get(string $id): ?Contact {
    $json = $this->getResource($id);

    if ($json === NULL) {
      return NULL;
    }

    return Contact::createFromJsonResponse($json->contact);
  }

  /**
   * Create a contact.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact to create.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Contact
   *   The new contact.
   *
   * @throws \JsonException
   */
  public function create(Contact $contact): Contact {
    $data = new \stdClass();
    $data->contact = clone $contact;

    unset($data->contact->id);

    foreach (array_keys($data->contact->fieldValues) as $index) {
      unset($data->contact->fieldValues[$index]->contact);
    }

    $json = $this->createResource($data);
    return Contact::createFromJsonResponse($json->contact);
  }

  /**
   * Update a contact.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact to update.
   */
  public function update(Contact $contact): void {
    if (empty($contact->id)) {
      throw new \InvalidArgumentException('ID is empty');
    }
    $data = new \stdClass();
    $data->contact = clone $contact;

    foreach (array_keys($data->contact->fieldValues) as $index) {
      unset($data->contact->fieldValues[$index]->contact);
    }

    $this->updateResource($data->contact->id, $data);
  }

  /**
   * List contacts.
   *
   * @param array $filters
   *   Filters to apply.
   * @param int $offset
   *   The pagination offset.
   * @param int|null $limit
   *   The pagination limit or null to get all.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Contact[]
   *   The list of contacts.
   */
  public function list(array $filters, int $offset = 0, int $limit = NULL): array {
    $list_filters = [];

    foreach ($filters as $key => $value) {
      $list_filters['filters[' . $key . ']'] = $value;
    }

    $json = $this->listResources($list_filters, $offset, $limit);

    return Contact::createFromJsonListResponse($json->contacts);
  }

  /**
   * Get a list of contact lists a contact is a member of.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact.
   *
   * @return \Drupal\activecampaign_api\ApiResource\ContactListMembership[]
   *   The memberships.
   */
  public function getContactListMemberships(Contact $contact): array {
    $url = $this->url . '/' . $contact->id . '/contactLists';
    $options = [
      'headers' => [
        'Api-Token' => $this->activecampaignApiAccount->getApiToken(),
      ],
    ];

    try {
      $response = $this->httpClient->get($url, $options);

      $contents = $response->getBody()->getContents();
      $json = json_decode($contents, FALSE, 512, JSON_THROW_ON_ERROR);
      return ContactListMembership::createFromJsonListResponse($json->contactLists);
    }
    catch (\Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode(), $e);
    }
  }

}
