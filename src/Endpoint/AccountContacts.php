<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Account;
use Drupal\activecampaign_api\ApiResource\AccountContact;
use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class AccountContacts.
 *
 * The AccountContacts endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class AccountContacts extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('accountContacts', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * Get a account.
   *
   * @param string $id
   *   The ID.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountContact|null
   *   The account contact or null if not found
   */
  public function get(string $id): ?AccountContact {
    $json = $this->getResource($id);

    if ($json === NULL) {
      return NULL;
    }

    return AccountContact::createFromJsonResponse($json->accountContact);
  }

  /**
   * Create a account.
   *
   * @param \Drupal\activecampaign_api\ApiResource\AccountContact $accountContact
   *   The account contact to create.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountContact
   *   The new account contact.
   *
   * @throws \JsonException
   */
  public function create(AccountContact $accountContact): AccountContact {
    $data = new \stdClass();
    $data->accountContact = clone $accountContact;

    unset($data->accountContact->id);

    $json = $this->createResource($data);
    return AccountContact::createFromJsonResponse($json->accountContact);
  }

  /**
   * Update a account.
   *
   * @param \Drupal\activecampaign_api\ApiResource\AccountContact $accountContact
   *   The account contact to update.
   */
  public function update(AccountContact $accountContact): void {
    if (empty($accountContact->id)) {
      throw new \InvalidArgumentException('ID is empty');
    }
    $data = new \stdClass();
    $data->accountContact = clone $accountContact;

    $this->updateResource($data->accountContact->id, $data);
  }

  /**
   * List accounts.
   *
   * @param array $filters
   *   Filters to apply.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountContact[]
   *   The list of account contacts.
   */
  protected function list(array $filters): array {
    $list_filters = [];

    foreach ($filters as $key => $value) {
      $list_filters['filters[' . $key . ']'] = $value;
    }

    $json = $this->listResources($list_filters);

    return AccountContact::createFromJsonListResponse($json->accountContacts);
  }

  /**
   * Get by account and contact.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Account $account
   *   The account.
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountContact|null
   *   The AccountContact.
   */
  public function getByAccountContact(Account $account, Contact $contact): ?AccountContact {
    $account_contacts = $this->list([
      'contact' => $contact->id,
      'account' => $account->id,
    ]);

    if (count($account_contacts) > 1) {
      throw new \RuntimeException('Count > 1');
    }
    if (count($account_contacts) === 0) {
      return NULL;
    }
    return $account_contacts[0];
  }

  /**
   * Get all by account.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Account $account
   *   The account.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountContact[]
   *   The list of account contacts.
   */
  public function getAllByAccount(Account $account): array {
    return $this->list(['account' => $account->id]);
  }

  /**
   * Get all by contact.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Contact $contact
   *   The contact.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountContact[]
   *   The list of account contacts.
   */
  public function getAllByContact(Contact $contact): array {
    return $this->list(['contact' => $contact->id]);
  }

}
