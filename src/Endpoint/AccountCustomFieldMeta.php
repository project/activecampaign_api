<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta as AccountCustomFieldMetaResource;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class AccountCustomFieldMeta.
 *
 * The AccountCustomFieldMeta endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class AccountCustomFieldMeta extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('accountCustomFieldMeta', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * Get a field.
   *
   * @param string $id
   *   The ID.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta|null
   *   The field or null if not found.
   */
  public function get(string $id): ?AccountCustomFieldMetaResource {
    $json = $this->getResource($id);

    if ($json === NULL) {
      return NULL;
    }

    return AccountCustomFieldMetaResource::getTypedInstance($json->accountCustomFieldMetum);
  }

  /**
   * Get a field by its field label.
   *
   * @param string $fieldLabel
   *   The field label.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta|null
   *   The field or null if not found.
   */
  public function getByFieldLabel(string $fieldLabel): ?AccountCustomFieldMetaResource {
    static $cache = NULL;
    if ($cache === NULL) {
      $cache = [];
      foreach ($this->list([], 0, NULL) as $item) {
        $cache[$item->fieldLabel] = $item;
      }
    }

    return $cache[$fieldLabel] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $id): void {
    throw new \RuntimeException('Not implemented');
  }

  /**
   * List fields.
   *
   * @param array $filters
   *   Filters to apply.
   * @param int $offset
   *   The pagination offset.
   * @param int|null $limit
   *   The pagination limit or null to get all.
   *
   * @return \Drupal\activecampaign_api\ApiResource\AccountCustomFieldMeta[]
   *   The list of fields.
   */
  public function list(array $filters, int $offset = 0, int $limit = NULL): array {
    $list_filters = [];

    foreach ($filters as $key => $value) {
      $list_filters['filters[' . $key . ']'] = $value;
    }

    $json = $this->listResources($list_filters, $offset, $limit);

    $fields = [];
    foreach ($json->accountCustomFieldMeta as $field) {
      $fields[] = AccountCustomFieldMetaResource::getTypedInstance($field);
    }
    return $fields;
  }

}
