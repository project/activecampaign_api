<?php

namespace Drupal\activecampaign_api\Endpoint;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Tag;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class Tags.
 *
 * The Tags endpoint.
 *
 * @package Drupal\activecampaign_api\Endpoint
 */
class Tags extends Endpoint {

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler, ActivecampaignApiAccountInterface $activecampaignApiAccount) {
    parent::__construct('tags', $httpClient, $moduleHandler, $activecampaignApiAccount);
  }

  /**
   * Get a tag.
   *
   * @param string $id
   *   The ID.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Tag|null
   *   The tag or null if not found
   */
  public function get(string $id): ?Tag {
    $json = $this->getResource($id);

    if ($json === NULL) {
      return NULL;
    }

    return Tag::getTypedInstance($json->tag);
  }

  /**
   * List tags.
   *
   * @param string|null $query
   *   Filter tags with this search query.
   * @param int $offset
   *   The pagination offset.
   * @param int|null $limit
   *   The pagination limit or null to get all.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Tag[]
   *   The list of tags.
   */
  public function list($query = NULL, int $offset = 0, int $limit = NULL): array {
    $list_filters = [];

    if (!empty($query)) {
      $list_filters['search'] = $query;
    }

    $json = $this->listResources($list_filters, $offset, $limit);

    $tags = [];
    foreach ($json->tags as $tag) {
      $tags[] = Tag::getTypedInstance($tag);
    }
    return $tags;
  }

  /**
   * Create a tag.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Tag $tag
   *   The tag to create.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Tag
   *   The new tag.
   *
   * @throws \JsonException
   */
  public function create(Tag $tag): Tag {
    $data = new \stdClass();
    $data->tag = clone $tag;
    $data->tag->tagType = Tag::getTagTypeForClass($tag);

    unset($data->tag->id);

    $json = $this->createResource($data);
    return Tag::getTypedInstance($json->tag);
  }

  /**
   * Update a tag.
   *
   * @param \Drupal\activecampaign_api\ApiResource\Tag $tag
   *   The tag to update.
   */
  public function update(Tag $tag): void {
    if (empty($tag->id)) {
      throw new \InvalidArgumentException('ID is empty');
    }
    $data = new \stdClass();
    $data->tag = clone $tag;
    $data->tag->tagType = Tag::getTagTypeForClass($tag);

    $this->updateResource($data->tag->id, $data);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $id): void {
    $this->deleteResource($id);
  }

}
