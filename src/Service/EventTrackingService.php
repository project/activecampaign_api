<?php

namespace Drupal\activecampaign_api\Service;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\Exception;
use GuzzleHttp\Client;

/**
 * Class EventTrackingService.
 *
 * Implements the AC event tracking service.
 *
 * @package Drupal\activecampaign_api\Service
 */
class EventTrackingService implements EventTrackingServiceInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * EventTrackingService constructor.
   *
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   */
  public function __construct(Client $httpClient) {
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public function track(ActivecampaignApiAccountInterface $activecampaignApiAccount, string $name, string $email, string $data = NULL): void {
    $visit = [
      'email' => $email,
    ];

    $query = [
      'key' => $activecampaignApiAccount->getEventTrackingKey(),
      'actid' => $activecampaignApiAccount->getEventTrackingActId(),
      'event' => $name,
      'visit' => json_encode($visit),
    ];

    if (!empty($data)) {
      $query['eventdata'] = $data;
    }

    $url = $activecampaignApiAccount->getEventTrackingBaseUrl() . '?' . http_build_query($query);

    $options = [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
    ];

    try {
      $response = $this->httpClient->post($url, $options);
      $contents = $response->getBody()->getContents();
      if ($response->getStatusCode() !== 200) {
        throw new Exception('Response !== 200: ' . $contents);
      }
      $json = json_decode($contents, FALSE, 512, JSON_THROW_ON_ERROR);

      if (!isset($json->success) || $json->success != 1) {
        throw new Exception('Failed: ' . $contents);
      }
    }
    catch (\Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode(), $e);
    }
  }

}
