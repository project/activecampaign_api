<?php

namespace Drupal\activecampaign_api\Service;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;

/**
 * Interface EventTrackingServiceInterface.
 *
 * Interface for ActiveCampaign event tracking.
 *
 * @package Drupal\activecampaign_api\Service
 */
interface EventTrackingServiceInterface {

  /**
   * Track an event.
   *
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param string $name
   *   The event name.
   * @param string $email
   *   The e-mail address of the contact this event is tracked for.
   * @param string|null $data
   *   The data to store with the event.
   */
  public function track(ActivecampaignApiAccountInterface $activecampaignApiAccount, string $name, string $email, string $data = NULL): void;

}
