<?php

namespace Drupal\activecampaign_api\Service;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\Client;

/**
 * Class EndpointFactory.
 *
 * The endpoint factory service.
 *
 * @package Drupal\activecampaign_api
 */
class EndpointFactory implements EndpointFactoryInterface {

  /**
   * Cached endpoints.
   *
   * @var \Drupal\activecampaign_api\Endpoint[]
   */
  protected $endpoints = [];

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The activecampaign api account.
   *
   * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface
   */
  protected $activecampaignApiAccount;

  /**
   * EndpointFactory constructor.
   *
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(Client $httpClient, ModuleHandlerInterface $moduleHandler) {
    $this->httpClient = $httpClient;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function setActivecampaignApiAccount(ActivecampaignApiAccountInterface $activecampaignApiAccount): void {
    $this->activecampaignApiAccount = $activecampaignApiAccount;
    foreach ($this->endpoints as $endpoint) {
      $endpoint->setActivecampaignApiAccount($this->activecampaignApiAccount);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $endpoint): Endpoint {

    if (empty($this->activecampaignApiAccount)) {
      throw new \RuntimeException('ActiveCampaign API account not set. Please call setActivecampaignApiAccount first.');
    }

    if (isset($this->endpoints[$endpoint])) {
      return $this->endpoints[$endpoint];
    }

    if (!class_exists($endpoint, TRUE)) {
      throw new \InvalidArgumentException('Endpoint doesnt exist.');
    }

    if (!is_subclass_of($endpoint, Endpoint::class)) {
      throw new \InvalidArgumentException('Endpoint is not valid.');
    }

    $this->endpoints[$endpoint] = new $endpoint(
      $this->httpClient,
      $this->moduleHandler,
      $this->activecampaignApiAccount
    );

    return $this->endpoints[$endpoint];
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured(): bool {
    return $this->activecampaignApiAccount instanceof ActivecampaignApiAccountInterface && !empty($this->activecampaignApiAccount->getBaseUrl()) && !empty($this->activecampaignApiAccount->getApiToken());
  }

}
