<?php

namespace Drupal\activecampaign_api\Service;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\Endpoint;

/**
 * Interface EndpointFactoryInterface.
 *
 * Interface for the endpoint factory.
 *
 * @package Drupal\activecampaign_api\Service
 */
interface EndpointFactoryInterface {

  /**
   * Get an API endpoint instance.
   *
   * @param string $endpoint
   *   The endpoint class name.
   *
   * @return \Drupal\activecampaign_api\Endpoint
   *   the endpoint instance.
   */
  public function get(string $endpoint): Endpoint;

  /**
   * Check if all settings are configured properly.
   *
   * @return bool
   *   True or false.
   */
  public function isConfigured(): bool;

  /**
   * Set the activecampaign api account.
   *
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   */
  public function setActivecampaignApiAccount(ActivecampaignApiAccountInterface $activecampaignApiAccount): void;

}
