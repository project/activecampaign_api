<?php

namespace Drupal\activecampaign_api\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Base form for activecampaign api account edit forms.
 *
 * @internal
 */
class ActivecampaignApiAccountForm extends EntityForm implements ContainerInjectionInterface {

  use ConfigFormBaseTrait;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /**
     * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaign_api_account
     */
    $activecampaign_api_account = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $activecampaign_api_account->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $activecampaign_api_account->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => '\Drupal\activecampaign_api\Entity\ActivecampaignApiApiAccount::load',
      ],
      '#disabled' => !$activecampaign_api_account->isNew(),
    ];

    $form['base_url'] = [
      '#type' => 'url',
      '#required' => TRUE,
      '#title' => $this->t('ActiveCampaign API base url'),
      '#default_value' => $activecampaign_api_account->getBaseUrl(),
    ];

    $form['api_token'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('ActiveCampaign API token'),
      '#default_value' => $activecampaign_api_account->getApiToken(),
    ];

    $form['event_tracking_base_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Event tracking base url'),
      '#default_value' => $activecampaign_api_account->getEventTrackingBaseUrl(),
    ];

    $form['event_tracking_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event tracking key'),
      '#default_value' => $activecampaign_api_account->getEventTrackingKey(),
    ];

    $form['event_tracking_actid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event tracking actid'),
      '#default_value' => $activecampaign_api_account->getEventTrackingActId(),
    ];

    $form['endpoint_create_update_delete_error_reporting_webhook_url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL to report (POST) create/update/delete errors to'),
      '#default_value' => $activecampaign_api_account->getEndpointCreateUpdateDeleteErrorReportingWebhookUrl(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    $form = parent::afterBuild($element, $form_state);
    $form['actions']['#weight'] = 1000;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $activecampaign_api_account = $this->entity;
    $status = $activecampaign_api_account->save();

    if ($status == SAVED_UPDATED) {
      $this->messenger()
        ->addStatus($this->t('Activecampaign api account %label has been updated.', ['%label' => $activecampaign_api_account->label()]));
      $this->logger('activecampaign_api')
        ->notice('Activecampaign api account %label has been updated.', [
          '%label' => $activecampaign_api_account->label(),
        ]);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('Activecampaign api account %label has been added.', ['%label' => $activecampaign_api_account->label()]));
      $this->logger('activecampaign_api')
        ->notice('Activecampaign api account %label has been added.', [
          '%label' => $activecampaign_api_account->label(),
        ]);
    }

    $url = Url::fromRoute('entity.activecampaign_api_account.collection');
    $form_state->setRedirectUrl($url);

    return $status;
  }

}
