<?php

namespace Drupal\activecampaign_api\Form;

use Drupal\activecampaign_api\Endpoint\Fields;
use Drupal\activecampaign_api\Service\EndpointFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ListFieldsForm.
 *
 * Form to list fields in activecampaign_api.
 *
 * @package Drupal\activecampaign_api\Form
 */
class ListFieldsForm extends FormBase {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The endpoint factory.
   *
   * @var \Drupal\activecampaign_api\Service\EndpointFactoryInterface
   */
  protected $endpointFactory;

  /**
   * TestForm constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\activecampaign_api\Service\EndpointFactoryInterface $endpointFactory
   *   The endpoint factory.
   */
  public function __construct(StateInterface $state, EntityTypeManagerInterface $entityTypeManager, EndpointFactoryInterface $endpointFactory) {
    $this->state = $state;
    $this->entityTypeManager = $entityTypeManager;
    $this->endpointFactory = $endpointFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('activecampaign_api.endpoint_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activecampaign_api_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $activecampaign_api_account_storage = $this->entityTypeManager->getStorage('activecampaign_api_account');
    $activecampaign_api_account_id = $this->state->get('activecampaign_api_account');

    $activecampaign_api_accounts = [];
    foreach ($activecampaign_api_account_storage->loadMultiple() as $activecampaign_api_account_entity) {
      $activecampaign_api_accounts[$activecampaign_api_account_entity->id()] = $activecampaign_api_account_entity->label();
    }

    if (empty($activecampaign_api_accounts)) {
      $form['no_accounts'] = [
        '#markup' => $this->t('No activecampaign api accounts configured.'),
      ];
      return $form;
    }

    $form['activecampaign_api_account'] = [
      '#type' => 'select',
      '#title' => $this->t('Activecampaign api account'),
      '#options' => $activecampaign_api_accounts,
      '#required' => TRUE,
      '#default_value' => $activecampaign_api_account_id,
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run'),
      '#button_type' => 'primary',
    ];

    $results = NULL;
    if ($form_state->getTriggeringElement() !== NULL) {
      /**
       * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaign_api_account
       */
      $activecampaign_api_account = $activecampaign_api_account_storage->load($form_state->getValue('activecampaign_api_account'));

      try {
        $this->endpointFactory->setActivecampaignApiAccount($activecampaign_api_account);
        /**
         * @var \Drupal\activecampaign_api\Endpoint\Fields $fields_endpoint
         */
        $fields_endpoint = $this->endpointFactory->get(Fields::class);

        $form['fields'] = [
          '#type' => 'table',
          '#weight' => 1000,
          '#header' => [
            $this->t('ID'),
            $this->t('Title'),
            $this->t('Type'),
          ],
          '#rows' => [],
          '#empty' => $this->t('No fields.'),
        ];

        foreach ($fields_endpoint->list([]) as $field) {
          $form['fields']['#rows'][] = [
            $field->id,
            $field->title,
            get_class($field),
          ];
        }

        usort($form['fields']['#rows'], function ($a, $b) {
          if ($a[0] == $b[0]) {
            return 0;
          }
          return ($a[0] < $b[0]) ? -1 : 1;
        });
      }
      catch (\Exception $e) {
        $form['fields'] = [
          '#markup' => $results = get_class($e) . ': ' . $e->getMessage() . ' at ' . $e->getFile() . ':' . $e->getLine() . "\n\n" . $e->getTraceAsString(),
          '#weight' => 1000,
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    $this->state->set('activecampaign_api_account', $form_state->getValue('activecampaign_api_account'));
  }

}
