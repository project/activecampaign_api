<?php

namespace Drupal\activecampaign_api\Form;

use Drupal\activecampaign_api\Endpoint;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestForm.
 *
 * Form to test activecampaign_api.
 *
 * @package Drupal\activecampaign_api\Form
 */
class TestForm extends FormBase {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * TestForm constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(StateInterface $state, Client $httpClient, ModuleHandlerInterface $moduleHandler, EntityTypeManagerInterface $entityTypeManager) {
    $this->state = $state;
    $this->httpClient = $httpClient;
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('http_client'),
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activecampaign_api_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $activecampaign_api_account_storage = $this->entityTypeManager->getStorage('activecampaign_api_account');
    $resource = $this->state->get('activecampaign_api_resource');
    $resource_id = $this->state->get('activecampaign_api_resource_id');
    $activecampaign_api_account_id = $this->state->get('activecampaign_api_account');

    $activecampaign_api_accounts = [];
    foreach ($activecampaign_api_account_storage->loadMultiple() as $activecampaign_api_account_entity) {
      $activecampaign_api_accounts[$activecampaign_api_account_entity->id()] = $activecampaign_api_account_entity->label();
    }

    if (empty($activecampaign_api_accounts)) {
      $form['no_accounts'] = [
        '#markup' => $this->t('No activecampaign api accounts configured.'),
      ];
      return $form;
    }

    $form['activecampaign_api_account'] = [
      '#type' => 'select',
      '#title' => $this->t('Activecampaign api account'),
      '#options' => $activecampaign_api_accounts,
      '#required' => TRUE,
      '#default_value' => $activecampaign_api_account_id,
    ];

    $form['resource'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource'),
      '#default_value' => $resource,
      '#required' => TRUE,
    ];

    $form['resource_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID'),
      '#default_value' => $resource_id,
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run'),
      '#button_type' => 'primary',
    ];

    $results = NULL;
    if ($form_state->getTriggeringElement() !== NULL) {
      /**
       * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaign_api_account
       */
      $activecampaign_api_account = $activecampaign_api_account_storage->load($form_state->getValue('activecampaign_api_account'));

      try {
        $endpoint = new Endpoint(
          $form_state->getValue('resource'),
          $this->httpClient,
          $this->moduleHandler,
          $activecampaign_api_account
        );

        $results = $endpoint->get($form_state->getValue('resource_id'));

        if ($results === NULL) {
          $results = 'NULL';
        }
      }
      catch (\Exception $e) {
        $results = get_class($e) . ': ' . $e->getMessage() . ' at ' . $e->getFile() . ':' . $e->getLine() . "\n\n" . $e->getTraceAsString();
      }

      $form['results'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<strong>Results</strong><pre>@results</pre>', [
          '@results' => print_r($results, TRUE),
        ]),
        '#weight' => 1000,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);

    $this->state->set('activecampaign_api_resource', $form_state->getValue('resource'));
    $this->state->set('activecampaign_api_resource_id', $form_state->getValue('resource_id'));
    $this->state->set('activecampaign_api_account', $form_state->getValue('activecampaign_api_account'));
  }

}
