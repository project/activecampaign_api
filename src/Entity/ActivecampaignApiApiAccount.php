<?php

namespace Drupal\activecampaign_api\Entity;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the activecampaign account entity.
 *
 * @ConfigEntityType(
 *   id = "activecampaign_api_account",
 *   label = @Translation("ActiveCampaign API account"),
 *   label_collection = @Translation("ActiveCampaign API accounts"),
 *   label_singular = @Translation("ActiveCampaign API account"),
 *   label_plural = @Translation("ActiveCampaign APIaccounts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ActiveCampaign API account",
 *     plural = "@count ActiveCampaign API accounts",
 *   ),
 *   handlers = {
 *     "access" =
 *   "Drupal\activecampaign_api\ActivecampaignApiAccountAccessControlHandler",
 *     "list_builder" =
 *   "Drupal\activecampaign_api\ActivecampaignApiAccountListBuilder",
 *     "form" = {
 *       "add" = "Drupal\activecampaign_api\Form\ActivecampaignApiAccountForm",
 *       "edit" =
 *   "Drupal\activecampaign_api\Form\ActivecampaignApiAccountForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   admin_permission = "manage activecampaign_api settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" =
 *   "/admin/config/services/activecampaign-api/account/{activecampaign_api_account}/delete",
 *     "edit-form" =
 *   "/admin/config/services/activecampaign-api/account/{activecampaign_api_account}",
 *     "collection" = "/admin/config/services/activecampaign-api/account"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "base_url",
 *     "api_token",
 *     "event_tracking_base_url",
 *     "event_tracking_key",
 *     "event_tracking_actid",
 *     "endpoint_create_update_delete_error_reporting_webhook_url"
 *   }
 * )
 */
class ActivecampaignApiApiAccount extends ConfigEntityBase implements ActivecampaignApiAccountInterface {

  /**
   * The acccount ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of the account.
   *
   * @var string
   */
  protected $label;

  /**
   * ActiveCampaign API base url.
   *
   * @var string|null
   */
  protected $base_url;

  /**
   * ActiveCampaign API token.
   *
   * @var string|null
   */
  protected $api_token;

  /**
   * Event tracking base url.
   *
   * @var string|null
   */
  protected $event_tracking_base_url;

  /**
   * Event tracking key.
   *
   * @var string|null
   */
  protected $event_tracking_key;

  /**
   * Event tracking actid.
   *
   * @var string|null
   */
  protected $event_tracking_actid;

  /**
   * URL to report (POST) create/update/delete errors to.
   *
   * @var string|null
   */
  protected $endpoint_create_update_delete_error_reporting_webhook_url;

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl(): ?string {
    return $this->base_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiToken(): ?string {
    return $this->api_token;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventTrackingBaseUrl(): ?string {
    return $this->event_tracking_base_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventTrackingKey(): ?string {
    return $this->event_tracking_key;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventTrackingActId(): ?string {
    return $this->event_tracking_actid;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpointCreateUpdateDeleteErrorReportingWebhookUrl(): ?string {
    return $this->endpoint_create_update_delete_error_reporting_webhook_url;
  }

}
