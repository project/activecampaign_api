<?php

/**
 * @file
 * Hooks provided by the activecampaign_api module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the data passed to ActiveCampaign when creating a resource.
 *
 * @param object $data
 *   The data.
 * @param \Drupal\activecampaign_api\Endpoint $endpoint
 *   The endpoint being called.
 */
function hook_activecampaign_api_endpoint_createresource_alter(object &$data, \Drupal\activecampaign_api\Endpoint $endpoint) {

}

/**
 * Alter the data passed to ActiveCampaign when updating a resource.
 *
 * @param object $data
 *   The data.
 * @param \Drupal\activecampaign_api\Endpoint $endpoint
 *   The endpoint being called.
 */
function hook_activecampaign_api_endpoint_updateresource_alter(object &$data, \Drupal\activecampaign_api\Endpoint $endpoint) {

}

/**
 * Alter the data being sent to the reporting webhook url.
 *
 * @param object $data
 *   The data.
 * @param \Drupal\activecampaign_api\Endpoint $endpoint
 *   The endpoint being called.
 */
function hook_activecampaign_api_report_error_to_webhook_alter(object &$data, \Drupal\activecampaign_api\Endpoint $endpoint) {

}

/**
 * @} End of "addtogroup hooks".
 */
